package main

import (
	"flag"
	"fmt"
	"github.com/ant0ine/go-json-rest/rest"
	"golang.org/x/net/websocket"
	controller "kingbloc.dts/controllers"
	"kingbloc.dts/services"
	"kingbloc.dts/tools"
	"kingbloc.util/util"
	"net/http"
	"os"
)

var hostAndPort string

func init() {
	var wd, _ = os.Getwd()
	var configFileName string
	flag.StringVar(&configFileName, "conf", "default.dts.ini", "running conf file.")
	flag.Parse()

	// confPath := wd + util.GetSysSplit() + util.GetSysSplit() + configFileName
	confPath := wd + util.GetSysSplit() + "kingbloc.dts" + util.GetSysSplit() + configFileName

	conf := tool.SetConfigPath(confPath)
	hostAndPort = ":" + conf.Get("sysconf", "port")
	fmt.Printf("dts running in %v, %v\n", hostAndPort, confPath)
	// db
	dbtype := conf.Get("database", "dbtype")
	dburl := conf.Get("database", "url")
	dbuname := conf.Get("database", "name")
	dbpwd := conf.Get("database", "password")
	isShow := conf.Get("database", "showSQL")

	tool.RegDB(dbtype, dburl, dbuname, dbpwd, isShow)
	//
	enableMsg := conf.Get("sysconf", "msgEnable")
	if enableMsg == "true" {
		service.InitProducer("127.0.0.1:4150")
		service.InitConsumer("127.0.0.1:4150", "test-channel", "test")
	}
	//
	isInitTables := conf.Get("sysconf", "initializedDatabase")
	if isInitTables == "true" {
		service.InitTablesDefaultData()
	}

}

func main() {
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	api.Use(&rest.ContentTypeCheckerMiddleware{})
	api.Use(&rest.CorsMiddleware{
		RejectNonCorsRequests: false,
		OriginValidator: func(origin string, request *rest.Request) bool {
			stat := true
			switch origin {
			case "http://127.0.0.1:8181":
				stat = true
			case "http://localhost":
				stat = true
			case "http://dts.kingbloc.com":
				stat = true
			case "http://dts-test.kingbloc.com":
				stat = true
			}
			return stat
		},
		AllowedMethods: []string{"POST"},
		AllowedHeaders: []string{
			"Accept", "Content-Type", "X-Custom-Header", "Origin"},
		AccessControlAllowCredentials: true,
		AccessControlMaxAge:           3600,
	})
	api.Use(&rest.JsonpMiddleware{
		CallbackNameKey: "ok",
	})

	//
	enableMsg := tool.GetConfig().Get("sysconf", "msgEnable")
	if enableMsg == "true" {
		http.Handle("/ws", websocket.Handler(service.Echo))
	}

	http.HandleFunc("/approve", controller.Approve)                  // 确认申请
	http.HandleFunc("/approves", controller.Approves)                // 我的审批列表
	http.HandleFunc("/myapplys", controller.MyApproves)              // 我的申请
	http.HandleFunc("/updatepwd2", controller.UpdatePwd2)            // 二级密码修改
	http.HandleFunc("/updatepwd1", controller.UpdatePwd1)            // 一级密码修改
	http.HandleFunc("/grantNewOrder", controller.GrantNewOrder)      // 报单授权
	http.HandleFunc("/myfriends", controller.MyFriends)              // 我的朋友圈
	http.HandleFunc("/newOrder", controller.NewOrder)                // 报单
	http.HandleFunc("/myDirectOrders", controller.MyDirectiveOrders) // 我直接推荐的单子
	http.HandleFunc("/myAccount", controller.MyAccount)              // 我的双轨制业务账户
	http.HandleFunc("/calcAward", controller.CalcAward)              // 计算所有奖项的奖金
	http.Handle("/", api.MakeHandler())
	err := http.ListenAndServe(hostAndPort, nil)
	fmt.Printf("%v\n", err)

}
