package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"kingbloc.sso/common"

	"kingbloc.util/util"
)

var hostAndPort string

func init() {

	var wd, _ = os.Getwd()
	var confFileName string
	flag.StringVar(&confFileName, "conf", "default.sso.ini", "running conf file.")
	flag.Parse()

	confPath := wd + util.GetSysSplit() + "kingbloc.sso" + util.GetSysSplit() + confFileName
	conf := common.SetConfig(confPath)

	hostAndPort = conf.Get("sysinfo", "port")
	if conf.Get("sysinfo", "showSQL") == "true" {
		util.InitDB(conf, true)
	} else {
		util.InitDB(conf, false)
	}

	if conf.Get("sysinfo", "initializedDatabase") == "true" {
		common.InitUser()
	}
	fmt.Printf("sso running in %v, %v\n", hostAndPort, confPath)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/edit", common.Edit)
	mux.HandleFunc("/update", common.EditProperty)
	mux.HandleFunc("/reg", common.Signin)
	mux.HandleFunc("/login", common.Login)
	mux.HandleFunc("/logout", common.Logout)
	mux.HandleFunc("/restore", common.Restore)
	mux.HandleFunc("/find", common.Find)
	mux.HandleFunc("/byids", common.ByIds)
	mux.HandleFunc("/myinfo", common.Myinfo)
	info := http.ListenAndServe(hostAndPort, mux)
	fmt.Printf("%v---%v \n", hostAndPort, info)

}
